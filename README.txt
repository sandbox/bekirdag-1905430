Est gateway module for Turkish Banks for Drupal Commerce

This module allows you to set Credit Card payments using 
Est Gateway for Turkish Banks: Akbank, Finansbank, Halkbankasi, 
Isbankasi, Anadolubank for Drupal Commerce.

Setup:
1. Download and install the module, then enable it
2. Go to admin/commerce/config/payment-methods/add and choose Turkish Banks Est
gateway, choose a name and optionally a tag, save it
3. Go to admin/commerce/config/payment-methods and enter your merchant info, 
save your settings. 

And the credit card form for Turkish Banks Est gateway will automatically 
appear on checkout.

The module is using php class written by Ozgur Vatansever, but we also 
added currency information, since it was only making payments by Turkish 
Liras. But as far as we are informed from Est, you need to request 
multi-currency option from your bank to use other currencies.
