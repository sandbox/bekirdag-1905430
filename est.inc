<?php

/**
 * @file
 * Turkish Banks Est payment class for Drupal Commerce Est gateway.
 */

mb_internal_encoding("utf-8");

/**
 * Author: Ozgur Vatansever email: ozgurvt@gmail.com.
 *
 * Editor for drupal: Bekir Dag email: bekir@piyote.com.
 *
 * This library provides a pure PHP interface for the EST SanalPOS API.
 *
 * Feel free to report bugs, contribute, use in your open
 * source projects without permission.
 *
 * This library provides a pure PHP interface for the EST SanalPOS API.
 *
 * Base class for the EST interface. EST wraps Isbank,
 * and Akbank POS services.
 *
 * This class provides the methods to,
 * Build up and send a payment transaction,
 * Fetch the details of a transaction, cancel the order refund any,
 * amount from any order.
 *
 * @slug: is a parameter that indicates which pos service you are going to,
 * connected.
 * @company: is a string value which is your company id.
 * @name: is your username.
 * @password: is your password.
 */

class CommerceEstEST {
  // Can be among (akbank, finansbank, halkbank, isbank, anadolubank).
  protected $slug;
  // Merchant id.
  protected $company;
  protected $name;
  protected $password;
  // TRUE for debug mode.
  protected $debug;
  protected $credentials = NULL;
  public $rawResponse = NULL;
  public $rawRequest = NULL;

  protected static $banksDetails = array(
    "akbank" => array(
      "host" => "www.sanalakpos.com",
      "testhost" => "testsanalpos.est.com.tr",
      "listOrdersURL" => "/servlet/listapproved",
      "detailOrderURL" => "/servlet/cc5ApiServer",
      "cancelOrderURL" => "/servlet/cc5ApiServer",
      "returnOrderURL" => "/servlet/cc5ApiServer",
      "purchaseOrderURL" => "/servlet/cc5ApiServer",
    ),
    "finansbank" => array(
      "host" => "www.fbwebpos.com",
      "testhost" => "testsanalpos.est.com.tr",
      "listOrdersURL" => "/servlet/listapproved",
      "detailOrderURL" => "/servlet/cc5ApiServer",
      "cancelOrderURL" => "/servlet/cc5ApiServer",
      "returnOrderURL" => "/servlet/cc5ApiServer",
      "purchaseOrderURL" => "/servlet/cc5ApiServer",
    ),
    "halkbank" => array(
      "host" => "sanalpos.halkbank.com.tr",
      "testhost" => "testsanalpos.est.com.tr",
      "listOrdersURL" => "/servlet/listapproved",
      "detailOrderURL" => "/servlet/cc5ApiServer",
      "cancelOrderURL" => "/servlet/cc5ApiServer",
      "returnOrderURL" => "/servlet/cc5ApiServer",
      "purchaseOrderURL" => "/servlet/cc5ApiServer",
    ),
    "isbank" => array(
      "host" => "spos.isbank.com.tr",
      "testhost" => "testsanalpos.est.com.tr",
      "listOrdersURL" => "/servlet/listapproved",
      "detailOrderURL" => "/servlet/cc5ApiServer",
      "cancelOrderURL" => "/servlet/cc5ApiServer",
      "returnOrderURL" => "/servlet/cc5ApiServer",
      "purchaseOrderURL" => "/servlet/cc5ApiServer",
    ),
    "anadolubank" => array(
      "host" => "anadolusanalpos.est.com.tr",
      "testhost" => "testsanalpos.est.com.tr",
      "listOrdersURL" => "/servlet/listapproved",
      "detailOrderURL" => "/servlet/cc5ApiServer",
      "cancelOrderURL" => "/servlet/cc5ApiServer",
      "returnOrderURL" => "/servlet/cc5ApiServer",
      "purchaseOrderURL" => "/servlet/cc5ApiServer",
    ),
  );

  /**
   * Construction function.
   */
  public function __construct($slug, $company, $name, $password, $debug = TRUE) {
    $possible_slugs = array(
      "akbank",
      "finansbank",
      "isbank",
      "anadolubank",
      "halkbank",
    );
    // If the slug is not among the possible slugs, then immediately,
    // Throw an exception.
    if (!in_array($slug, $possible_slugs)) {
      throw new Exception("Ge�ersiz bir slug se�tiniz. Se�enekler: akbank, 
        isbank, finansbank, halkbank, anadolubank.");
    }

    $this->slug = $slug;
    $this->company = $company;
    $this->name = $name;
    $this->password = $password;
    $this->debug = ($debug == "false") ? FALSE : TRUE;
    $this->credentials = self::$banksDetails[$this->slug];
  }

  /**
   * Create credentials.
   */
  protected function getCredentials() {
    if ($this->credentials) {
      return $this->credentials;
    }
    if ($this->slug) {
      if (array_key_exists($this->slug, self::$banksDetails)) {
        return self::$banksDetails[$this->slug];
      }
      return NULL;
    }
    return NULL;
  }

  /**
   * Connect to the API server.
   */
  protected function connect() {
    if ($this->debug) {
      return "https://" . $this->credentials["testhost"];
    }
    else {
      return "https://" . $this->credentials["host"];
    }
  }

  /**
   * Sends a direct payment or pre authentication request.
   *
   * With the given order id and amount to the POS service.
   * Order ID is not necessary. If you don't send it, POS,
   * service creates it for you.
   * Amount must be greater than 0.0 TL. You can not issue 0 TL.
   * Returns True and the details if the payment is successful,
   * otherwise returns False.
   * 'extra' contains the purchase details like shipping and,
   * billing addresses, phone, email,
   *
   * @input: string x string x string x string x string x string x,
   * Decimal x int,
   * output: bool x dict,
   * Parameters:   ,
   *
   * @credit_card_number: 15-16 digits credit card number.
   * @cvv: 3-4 digits cvv number   ,
   * @year: 2 digits expiry year of the card (fmt: '12' for the year 2012),
   * @month: 2 digits expiry month of the card (fmt: '01' for January),
   * @amount: amount to be purchased,
   * @installment: taksit,
   * @orderid: Order ID ,
   * EXTRA:   ,
   * email = string,
   * ipaddress = string ,
   * userid = string,
   * billing_address_name       = string,
   * billing_address_street1    = string,
   * billing_address_street2    = string,
   * billing_address_street3    = string,
   * billing_address_city       = string,
   * billing_address_company    = string,
   * billing_address_postalcode = string,
   * billing_address_telvoice   = string,
   * billing_address_state      = string,
   * shipping_address_name       = string,
   * shipping_address_street1    = string,
   * shipping_address_street2    = string,
   * shipping_address_street3    = string,
   * shipping_address_city       = string,
   * shipping_address_company    = string,
   * shipping_address_postalcode = string,
   * shipping_address_state      = string,
   * shipping_address_telvoice   = string.
   */
  public function pay($credit_card_number, $cvv, $month, $year, $amount,
    $currency, $installment, $orderid, $typ = "Auth", $extra = array()) {

    $month = str_pad($month, 2, "0", STR_PAD_LEFT);
    $year = str_pad($year, 2, "0", STR_PAD_LEFT);
    $expires = $month . $year;
    $amount = number_format($amount, 2);
    $username = $this->name;
    $password = $this->password;
    $clientid = $this->company;
    $email = $this->getValue($extra, "email");
    $ipaddress = $this->getValue($extra, "ipaddress");
    $userid = $this->getValue($extra, "userid");
    $document = new CommerceEstXMLBuilder();
    $elements = array(
      "Name" => $username,
      "Password" => $password,
      "ClientId" => $clientid,
      "Mode" => "P",
      "OrderId" => $orderid,
      "Type" => $typ,
      "GroupId" => "",
      "TransId" => "",
      "UserId" => $userid,
      "Extra" => "",
      "Taksit" => $installment,
      "Number" => $credit_card_number,
      "Expires" => $expires,
      "Cvv2Val" => $cvv,
      "Total" => $amount,
      "Currency" => $currency,
      "Email" => $email,
      "IPAddress" => $ipaddress,
    );
    $dom_elements = $document->createElementsWithTextNodes($elements);
    $document->appendListOfElementsToElement($document->root(), $dom_elements);
    $billto = $document->createElement("BillTo");
    $billing_address_name = $this->getValue($extra, "billing_address_name");
    $billing_address_street1 = $this->getValue($extra, "billing_address_street1");
    $billing_address_street2 = $this->getValue($extra, "billing_address_street2");
    $billing_address_street3 = $this->getValue($extra, "billing_address_street3");
    $billing_address_city = $this->getValue($extra, "billing_address_city");
    $billing_address_company = $this->getValue($extra, "billing_address_company");
    $billing_address_postalcode = $this->getValue($extra, "billing_address_postalcode");
    $billing_address_telvoice = $this->getValue($extra, "billing_address_telvoice");
    $billing_address_state = $this->getValue($extra, "billing_address_state");

    $elements = array(
      "Name" => $billing_address_name,
      "Street1" => $billing_address_street1,
      "Street2" => $billing_address_street2,
      "Street3" => $billing_address_street3,
      "City" => $billing_address_city,
      "StateProv" => $billing_address_state,
      "PostalCode" => $billing_address_postalcode,
      "Country" => "Türkiye",
      "Company" => $billing_address_company,
      "TelVoice" => $billing_address_telvoice,
    );

    $dom_elements = $document->createElementsWithTextNodes($elements);
    $document->appendListOfElementsToElement($billto, $dom_elements);
    $document->root()->appendChild($billto);

    $shipto = $document->createElement("ShipTo");
    $shipping_address_name = $this->getValue($extra, "shipping_address_name");
    $shipping_address_street1 = $this->getValue($extra, "shipping_address_street1");
    $shipping_address_street2 = $this->getValue($extra, "shipping_address_street2");
    $shipping_address_street3 = $this->getValue($extra, "shipping_address_street3");
    $shipping_address_city = $this->getValue($extra, "shipping_address_city");
    $shipping_address_company = $this->getValue($extra, "shipping_address_company");
    $shipping_address_postalcode = $this->getValue($extra, "shipping_address_postalcode");
    $shipping_address_telvoice = $this->getValue($extra, "shipping_address_telvoice");
    $shipping_address_state = $this->getValue($extra, "shipping_address_state");

    $elements = array(
      "Name" => $shipping_address_name,
      "Street1" => $shipping_address_street1,
      "Street2" => $shipping_address_street2,
      "Street3" => $shipping_address_street3,
      "City" => $shipping_address_city,
      "StateProv" => $shipping_address_state,
      "PostalCode" => $shipping_address_postalcode,
      "Country" => "Türkiye",
      "Company" => $shipping_address_company,
      "TelVoice" => $shipping_address_telvoice,
    );

    $dom_elements = $document->createElementsWithTextNodes($elements);
    $document->appendListOfElementsToElement($shipto, $dom_elements);
    $document->root()->appendChild($shipto);
    $document_string = $document->saveXML();
    $this->rawRequest = $document_string;

    /* After the XML request has been created, we should now set the HTTP request using curl library..   */
    $url = $this->connect() . $this->credentials["purchaseOrderURL"];

    $curl = curl_init();
    $post_data = urlencode("DATA") . "=" . urlencode($document_string);
    // Set the url..
    curl_setopt($curl, CURLOPT_URL, $url);
    // Set the HTTP method to POST..
    curl_setopt($curl, CURLOPT_POST, 1);
    // Set the HTTP response header to False not to get the response header..
    curl_setopt($curl, CURLOPT_HEADER, FALSE);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    // Add the HTTP POST body..
    curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
    // Set the HTTP request header..
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      "Content-type" => "application/x-www-form-urlencoded",
      )
    );
    // Execute the request and save the response inside a variable
    // Called 'rawResponse'..
    $this->rawResponse = curl_exec($curl);
    // Close the connection..
    curl_close($curl);

    // After we got the response, we should now parse it using xml library..
    $response_dom_object = new DOMDocument();
    $response_dom_object->loadXML($this->rawResponse);
    // The result to be returned will be an array containing the response
    // Details.
    $result = array();
    try {
      $orderid = CommerceEstXMLBuilder::getData($response_dom_object, "OrderId");
      $groupid = CommerceEstXMLBuilder::getData($response_dom_object, "GroupId");
      $transid = CommerceEstXMLBuilder::getData($response_dom_object, "TransId");
      $response = CommerceEstXMLBuilder::getData($response_dom_object, "Response");
      $return_code = CommerceEstXMLBuilder::getData($response_dom_object,
        "ProcReturnCode");
      $error_msg = CommerceEstXMLBuilder::getData($response_dom_object, "ErrMsg");
      $host_msg = CommerceEstXMLBuilder::getData($response_dom_object, "HOSTMSG");
      $trx_date = CommerceEstXMLBuilder::getData($response_dom_object, "TRXDATE");
      $auth_code = CommerceEstXMLBuilder::getData($response_dom_object, "AuthCode");
      $is_successful = FALSE;
      if (intval($return_code) == 0) {
        $is_successful = TRUE;
      }
      $result["orderid"] = $orderid;
      $result["transid"] = $transid;
      $result["groupid"] = $groupid;
      $result["response"] = $response;
      $result["return_code"] = $return_code;
      $result["error_msg"] = $error_msg;
      $result["host_msg"] = $host_msg;
      $result["auth_code"] = $auth_code;
      $result["result"] = $is_successful;
    }
    catch(Exception $e){
      $result["result"] = FALSE;
      $result["exception"] = $e->getMessage();
    }

    if (isset($trx_date)) {
      try {
        $trx_date = explode(".", $trx_date);
        $trx_date = $trx_date[0];
        $trx_date = strptime($trx_date, "%Y%m%d %H:%M:%S");
        $result["transaction_time"] = $trx_date;
      }
      catch(Exception $e) {
        // Pass.
      }
    }

    return $result;
  }

  /**
   * Sends a cancel request for the given orderid to the POS service.
   *
   * If successful, returns True.
   * Otherwise returns False. Also gives the xml,
   * response sent from the POS service,
   * If you want to cancel a refund request or a postAuth,
   * request, then you must give the transaction ID.
   * to this method.
   *
   * @input: string x string.
   * @output: array.
   */
  public function cancel($orderid, $transid = NULL, $currency = 949) {
    $username = $this->name;
    $password = $this->password;
    $clientid = $this->company;

    $document = new CommerceEstXMLBuilder();
    $elements = array(
      "Name" => $username,
      "Password" => $password,
      "ClientId" => $clientid,
      "Mode" => "P",
      "OrderId" => $orderid,
      "Type" => "Void",
      "Currency" => $currency,
    );

    // Include the transaction id if the actual,
    // Parameter for 'transid' is not NULL..
    if ($transid) {
      $elements["TransId"] = $transid;
    }
    $dom_elements = $document->createElementsWithTextNodes($elements);
    $document->appendListOfElementsToElement($document->root(), $dom_elements);
    $document_string = $document->saveXML();
    $this->rawRequest = $document_string;

    /* After the XML request has been created,
    we should now set the HTTP request using curl library..   */
    $url = $this->connect() . $this->credentials["cancelOrderURL"];
    $curl = curl_init();
    $post_data = urlencode("DATA") . urlencode("=") . urlencode($document_string);
    // Set the url..
    curl_setopt($curl, CURLOPT_URL, $url);
    // Set the HTTP method to POST..
    curl_setopt($curl, CURLOPT_POST, 1);
    // Set the HTTP response header to False not to get the response header..
    curl_setopt($curl, CURLOPT_HEADER, FALSE);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    // Add the HTTP POST body..
    curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
    // Set the HTTP request header..
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      "Content-type" => "application/x-www-form-urlencoded",
      )
    );
    // Execute the request and save the response inside a
    // Variable called 'rawResponse'..
    $this->rawResponse = curl_exec($curl);
    // Close the connection..
    curl_close($curl);

    // After we got the response, we should now parse it using xml library..
    $response_dom_object = new DOMDocument();
    $response_dom_object->loadXML($this->rawResponse);
    // The result to be returned will be an array containing the
    // Response details..
    $result = array();
    try {
      $orderid = CommerceEstXMLBuilder::getData($response_dom_object, "OrderId");
      $groupid = CommerceEstXMLBuilder::getData($response_dom_object, "GroupId");
      $transid = CommerceEstXMLBuilder::getData($response_dom_object, "TransId");
      $response = CommerceEstXMLBuilder::getData($response_dom_object, "Response");
      $return_code = CommerceEstXMLBuilder::getData($response_dom_object,
         "ProcReturnCode");
      $error_msg = CommerceEstXMLBuilder::getData($response_dom_object, "ErrMsg");
      $host_msg = CommerceEstXMLBuilder::getData($response_dom_object, "HOSTMSG");
      $trx_date = CommerceEstXMLBuilder::getData($response_dom_object, "TRXDATE");
      $host_ref_num = CommerceEstXMLBuilder::getData($response_dom_object, "HostRefNum");
      $auth_code = CommerceEstXMLBuilder::getData($response_dom_object, "AuthCode");
      $is_successful = FALSE;
      if (intval($return_code) == 0) {
        $is_successful = TRUE;
      }
      $result["orderid"] = $orderid;
      $result["transid"] = $transid;
      $result["groupid"] = $groupid;
      $result["response"] = $response;
      $result["return_code"] = $return_code;
      $result["error_msg"] = $error_msg;
      $result["host_msg"] = $host_msg;
      $result["auth_code"] = $auth_code;
      $result["host_ref_num"] = $host_ref_num;
      $result["result"] = $is_successful;
    }
    catch(Exception $e){
      $result["result"] = FALSE;
      $result["exception"] = $e->getMessage();
    }

    if (isset($trx_date)) {
      try {
        $trx_date = explode(".", $trx_date);
        $trx_date = $trx_date[0];
        $trx_date = strptime($trx_date, "%Y%m%d %H:%M:%S");
        $result["transaction_time"] = $trx_date;
      }
      catch(Exception $e) {
        // Pass.
      }
    }
    return $result;
  }

  /**
   * Sends a refund request for the given orderid to the POS service.
   *
   * If successful, returns True.
   * Otherwise returns False. Also gives the xml response sent
   * from the POS service,
   *
   * @input: float x string
   * @output: array.
   */
  public function refund($amount, $orderid, $currency) {
    $username = $this->name;
    $password = $this->password;
    $clientid = $this->company;

    $amount = number_format($amount, 2);
    $document = new CommerceEstXMLBuilder();
    $elements = array(
      "Name" => $username,
      "Password" => $password,
      "ClientId" => $clientid,
      "Mode" => "P",
      "OrderId" => $orderid,
      "Type" => "Credit",
      "Currency" => $currency,
      "Total" => $amount,
    );
    $dom_elements = $document->createElementsWithTextNodes($elements);
    $document->appendListOfElementsToElement($document->root(), $dom_elements);
    $document_string = $document->saveXML();
    $this->rawRequest = $document_string;

    /* After the XML request has been created,
    we should now set the HTTP request
    using curl library..   */
    $url = $this->connect() . $this->credentials["returnOrderURL"];
    $curl = curl_init();
    $post_data = urlencode("DATA") . urlencode("=") . urlencode($document_string);
    // Set the url..
    curl_setopt($curl, CURLOPT_URL, $url);
    // Set the HTTP method to POST..
    curl_setopt($curl, CURLOPT_POST, 1);
    // Set the HTTP response header to False not to get the response header..
    curl_setopt($curl, CURLOPT_HEADER, FALSE);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    // Add the HTTP POST body..
    curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
    // Set the HTTP request header..
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      "Content-type" => "application/x-www-form-urlencoded",
      )
    );
    // Execute the request and save the response inside a
    // Variable called 'rawResponse'..
    $this->rawResponse = curl_exec($curl);
    // Close the connection..
    curl_close($curl);
    // After we got the response, we should now parse it using xml library..
    $response_dom_object = new DOMDocument();
    $response_dom_object->loadXML($this->rawResponse);
    // The result to be returned will be an array containing the
    // response details..
    $result = array();
    try {
      $orderid = CommerceEstXMLBuilder::getData($response_dom_object, "OrderId");
      $groupid = CommerceEstXMLBuilder::getData($response_dom_object, "GroupId");
      $transid = CommerceEstXMLBuilder::getData($response_dom_object, "TransId");
      $response = CommerceEstXMLBuilder::getData($response_dom_object, "Response");
      $return_code = CommerceEstXMLBuilder::getData($response_dom_object,
        "ProcReturnCode");
      $error_msg = CommerceEstXMLBuilder::getData($response_dom_object, "ErrMsg");
      $host_msg = CommerceEstXMLBuilder::getData($response_dom_object, "HOSTMSG");
      $trx_date = CommerceEstXMLBuilder::getData($response_dom_object, "TRXDATE");
      $host_ref_num = CommerceEstXMLBuilder::getData($response_dom_object, "HostRefNum");
      $auth_code = CommerceEstXMLBuilder::getData($response_dom_object, "AuthCode");
      $is_successful = FALSE;
      if (intval($return_code) == 0) {
        $is_successful = TRUE;
      }
      $result["orderid"] = $orderid;
      $result["transid"] = $transid;
      $result["groupid"] = $groupid;
      $result["response"] = $response;
      $result["return_code"] = $return_code;
      $result["error_msg"] = $error_msg;
      $result["host_msg"] = $host_msg;
      $result["auth_code"] = $auth_code;
      $result["host_ref_num"] = $host_ref_num;
      $result["result"] = $is_successful;
    }
    catch(Exception $e){
      $result["result"] = FALSE;
      $result["exception"] = $e->getMessage();
    }

    if (isset($trx_date)) {
      try {
        $trx_date = explode(".", $trx_date);
        $trx_date = $trx_date[0];
        $trx_date = strptime($trx_date, "%Y%m%d %H:%M:%S");
        $result["transaction_time"] = $trx_date;
      }
      catch(Exception $e) {
        // Pass.
      }
    }
    return $result;

  }

  /**
   * Issues a PostAuth request to the API server.
   *
   * Can be reversed by cancel() method,
   *
   * @input: float x string x string,
   * @output: array.
   */
  public function postAuth($amount, $orderid, $transid = NULL) {
    $username = $this->name;
    $password = $this->password;
    $clientid = $this->company;

    $amount = number_format($amount, 2);
    $document = new CommerceEstXMLBuilder();
    $elements = array(
      "Name" => $username,
      "Password" => $password,
      "ClientId" => $clientid,
      "Mode" => "P",
      "OrderId" => $orderid,
      "Type" => "PostAuth",
      "Total" => $amount,
      "Extra" => NULL,
      "TransId" => $transid,
    );
    $dom_elements = $document->createElementsWithTextNodes($elements);
    $document->appendListOfElementsToElement($document->root(), $dom_elements);
    $document_string = $document->saveXML();
    $this->rawRequest = $document_string;

    /* After the XML request has been created, we should now set the HTTP
    request using curl library..   */
    $url = $this->connect() . $this->credentials["purchaseOrderURL"];
    $curl = curl_init();
    $post_data = urlencode("DATA") . urlencode("=") . urlencode($document_string);
    // Set the url..
    curl_setopt($curl, CURLOPT_URL, $url);
    // Set the HTTP method to POST..
    curl_setopt($curl, CURLOPT_POST, 1);
    // Set the HTTP response header to False not to get the response header..
    curl_setopt($curl, CURLOPT_HEADER, FALSE);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    // Add the HTTP POST body..
    curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
    // Set the HTTP request header..
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      "Content-type" => "application/x-www-form-urlencoded",
      )
    );
    // Execute the request and save the response inside a
    // Variable called 'rawResponse'..
    $this->rawResponse = curl_exec($curl);
    // Close the connection..
    curl_close($curl);
    // After we got the response, we should now parse it using xml library..
    $response_dom_object = new DOMDocument();
    $response_dom_object->loadXML($this->rawResponse);
    // The result to be returned will be an array containing the
    // Response details..
    $result = array();
    try {
      $orderid = CommerceEstXMLBuilder::getData($response_dom_object, "OrderId");
      $groupid = CommerceEstXMLBuilder::getData($response_dom_object, "GroupId");
      $transid = CommerceEstXMLBuilder::getData($response_dom_object, "TransId");
      $response = CommerceEstXMLBuilder::getData($response_dom_object, "Response");
      $return_code = CommerceEstXMLBuilder::getData($response_dom_object, "ProcReturnCode");
      $error_msg = CommerceEstXMLBuilder::getData($response_dom_object, "ErrMsg");
      $host_msg = CommerceEstXMLBuilder::getData($response_dom_object, "HOSTMSG");
      $trx_date = CommerceEstXMLBuilder::getData($response_dom_object, "TRXDATE");
      $host_ref_num = CommerceEstXMLBuilder::getData($response_dom_object, "HostRefNum");
      $auth_code = CommerceEstXMLBuilder::getData($response_dom_object, "AuthCode");
      $is_successful = FALSE;
      if (intval($return_code) == 0) {
        $is_successful = TRUE;
      }
      $result["orderid"] = $orderid;
      $result["transid"] = $transid;
      $result["groupid"] = $groupid;
      $result["response"] = $response;
      $result["return_code"] = $return_code;
      $result["error_msg"] = $error_msg;
      $result["host_msg"] = $host_msg;
      $result["auth_code"] = $auth_code;
      $result["host_ref_num"] = $host_ref_num;
      $result["result"] = $is_successful;
    }
    catch(Exception $e){
      $result["result"] = FALSE;
      $result["exception"] = $e->getMessage();
    }

    if (isset($trx_date)) {
      try {
        $trx_date = explode(".", $trx_date);
        $trx_date = $trx_date[0];
        $trx_date = strptime($trx_date, "%Y%m%d %H:%M:%S");
        $result["transaction_time"] = $trx_date;
      }
      catch(Exception $e) {
        // Pass.
      }
    }
    return $result;
  }

  /**
   * Fetches the order detail through the POS XML API.
   *
   * The result is an array instance which contains, details of the order.
   *
   * @input: string,
   * @output: array.
   */
  public function getDetail($orderid) {
    $username = $this->name;
    $password = $this->password;
    $clientid = $this->company;

    $document = new CommerceEstXMLBuilder();
    $elements = array(
      "Name" => $username,
      "Password" => $password,
      "ClientId" => $clientid,
      "Mode" => "P",
      "OrderId" => $orderid,
    );
    $dom_elements = $document->createElementsWithTextNodes($elements);
    $document->appendListOfElementsToElement($document->root(), $dom_elements);
    $element = $document->createElement("Extra");
    $status_element = $document->createElementWithTextNode("ORDERSTATUS", "SOR");
    $element->appendChild($status_element);
    $document->root()->appendChild($element);
    $document_string = $document->saveXML();
    $this->rawRequest = $document_string;

    /* After the XML request has been created, we should now set the
    HTTP request using curl library..   */
    $url = $this->connect() . $this->credentials["detailOrderURL"];
    $curl = curl_init();
    $post_data = urlencode("DATA") . urlencode("=") . urlencode($document_string);
    // Set the url..
    curl_setopt($curl, CURLOPT_URL, $url);
    // Set the HTTP method to POST..
    curl_setopt($curl, CURLOPT_POST, 1);
    // Set the HTTP response header to False not to get the response header..
    curl_setopt($curl, CURLOPT_HEADER, FALSE);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    // Add the HTTP POST body..
    curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
    // Set the HTTP request header..
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      "Content-type" => "application/x-www-form-urlencoded",
      )
    );
    // Execute the request and save the response inside a
    // Variable called 'rawResponse'..
    $this->rawResponse = curl_exec($curl);
    // Close the connection..
    curl_close($curl);
    // After we got the response, we should now parse it using xml library..
    $response_dom_object = new DOMDocument();
    $response_dom_object->loadXML($this->rawResponse);
    // The result to be returned will be an array containing the
    // Response details..
    $result = array();

    $transid = CommerceEstXMLBuilder::getData($response_dom_object, "TransId");
    $return_code = CommerceEstXMLBuilder::getData($response_dom_object, "ProcReturnCode");
    $err_msg = CommerceEstXMLBuilder::getData($response_dom_object, "ErrMsg");
    $host_ref_num = CommerceEstXMLBuilder::getData($response_dom_object, "HOST_REF_NUM");
    $auth_code = CommerceEstXMLBuilder::getData($response_dom_object, "AUTH_CODE");
    $charge_type = CommerceEstXMLBuilder::getData($response_dom_object, "CHARGE_TYPE_CD");
    $capture_amount = CommerceEstXMLBuilder::getData($response_dom_object, "CAPTURE_AMT");
    $trx_date = CommerceEstXMLBuilder::getData($response_dom_object, "CAPTURE_DTTM");

    $result["transid"] = $transid;
    $result["orderid"] = $orderid;
    $result["return_code"] = $return_code;
    $result["host_ref_num"] = $host_ref_num;
    $result["error_msg"] = $err_msg;
    $result["charge_type"] = $charge_type;
    $result["auth_code"] = $auth_code;
    $result["amount"] = "";
    $result["transaction_time"] = "";

    if ($trx_date) {
      try {
        $trx_date = explode(".", $trx_date);
        $trx_date = $trx_date[0];
        $trx_date = strptime($trx_date, "%Y-%m-%d %H:%M:%S");
        $result["transaction_time"] = $trx_date;
      }
      catch(Exception $e) {
        // Pass.
      }
    }

    if ($capture_amount) {
      try {
        $capture_amount = intval($capture_amount) / 100.0;
        $result["amount"] = $capture_amount;
      }
      catch(Exception $e) {
        // Pass.
      }
    }
    return $result;

  }

  /**
   * Get value of the array.
   */
  protected function getValue($array, $key) {
    if (array_key_exists($key, $array)) {
      return $array[$key];
    }
    return NULL;
  }

  /**
   * Slug string convert.
   */
  public function toString() {
    return $this->slug . " sanalpos";
  }

}
