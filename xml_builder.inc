<?php

/**
 * @file
 * Turkish Banks Est XMLBuilder helper class.
 */

mb_internal_encoding("utf-8");

/**
 * This class wrapps around the DOMDocument class.
 *
 * And provides some utility functions to build up XML document easily.
 */
class CommerceEstXMLBuilder extends DOMDocument {
  public $rootElement;

  /**
   * This class wrapps around the Document class.
   *
   * And provides some utility functions to build up XML document easily.
   */
  public function __construct($tag = "CC5Request") {
    parent::__construct("1.0");
    $element = $this->createElement($tag);
    $this->rootElement = $element;
    $this->appendChild($this->rootElement);
  }

  /**
   * Gets the root element which is at the top of Dom hierarchy.
   */
  public function root() {
    return $this->rootElement;
  }

  /**
   * Creates a dom element with the given tag name and the node value.
   *
   * @input: string, string
   * @output: DOM Element
   */
  public function createElementWithTextNode($tag_name, $node_value) {
    if ($node_value == NULL) {
      $node_value = "";
    }
    $element = $this->createElement(strval($tag_name));
    $node = $this->createTextNode(strval($node_value));
    $element->appendChild($node);
    return $element;
  }

  /**
   * Creates a list of DOM Element instances with the given parameters.
   *
   * @input: string, string.
   * @output: [DOM Element, DOM Element, ...]
   */
  public function createElementsWithTextNodes($arguments) {
    $result_array = array();
    foreach ($arguments as $k => $v) {
      array_push($result_array, $this->createElementWithTextNode($k, $v));
    }
    return $result_array;
  }

  /**
   * Appends list of DOM elements to the given DOM element.
   */
  public function appendListOfElementsToElement($element, $elements) {
    foreach ($elements as $ele) {
      $element->appendChild($ele);
    }
  }

  /**
   * XML to string.
   */
  public function toString() {
    return $this->saveXML();
  }

  /**
   * Get XML Object data.
   */
  public static function getData($xml_obj, $tag) {
    $elements = $xml_obj->getElementsByTagName($tag);
    if ($elements->length > 0) {
      $item = $elements->item(0);
      $childiren = $item->childNodes;
      if ($childiren->length > 0) {
        return $childiren->item(0)->nodeValue;
      }
      return "";
    }
    return "";
  }
}
